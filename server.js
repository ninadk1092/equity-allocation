const express = require('express');

const tenantRoutes = require('./src/tenants/routes');
const stockRoutes = require('./src/stock-master/routes');
const userRoutes = require('./src/users/routes');

const app = express();
const port = 3000;

app.use(express.json());

app.get("/", (req, res) => {
    res.send("Hello World");
});

app.use("/api/v1/tenants", tenantRoutes);
app.use("/api/v1/stocks", stockRoutes);
app.use("/api/v1/tenants/users", userRoutes);

app.listen(port, () => console.log(`app listening on port ${port}`))