const pool = require('../../db');

const queries = require('./queries');

const getStocks = (req, res) => {
    pool.query(queries.getStocks, (error, results) => {
        if (error) throw error;
        res.status(200).json(results.rows);
    });
};

const getStocksByID = (req, res) => {
    const id = parseInt(req.params.id);
    pool.query(queries.getStocksByID, [id], (error, results) => {
        if (error) throw error;
        res.status(200).json(results.rows);
    });
};

module.exports = {
    getStocks,
    getStocksByID,
};