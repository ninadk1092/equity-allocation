const getStocks = "SELECT * FROM stock_master";

const getStocksByID = "SELECT * FROM stock_master WHERE id = $1";

module.exports = {
    getStocks,
    getStocksByID,
};