const pool = require('../../db');

const queries = require('./queries');

const getTenants = (req, res) => {
    pool.query(queries.getTenants, (error, results) => {
        if (error) throw error;
        res.status(200).json(results.rows);
    });
};

const getTenantByID = (req, res) => {
    const id = parseInt(req.params.id);
    pool.query(queries.getTenantByID, [id], (error, results) => {
        if (error) throw error;
        res.status(200).json(results.rows);
    });
};

module.exports = {
    getTenants,
    getTenantByID,
};