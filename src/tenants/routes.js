const { Router } = require('express');

const controller = require('./controller');

const router = Router();

router.get('/', controller.getTenants);
router.get('/:id', controller.getTenantByID);

module.exports = router;
