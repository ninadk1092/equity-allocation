const getTenants = "SELECT * FROM tenants";

const getTenantByID = "SELECT * FROM tenants WHERE id = $1";

module.exports = {
    getTenants,
    getTenantByID,
};