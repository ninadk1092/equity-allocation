const { Router } = require('express');

const controller = require('./controller');

const router = Router();

router.get('/:tenantID', controller.getUsers);
router.get('/:tenantID/:id', controller.getUserByID);

module.exports = router;