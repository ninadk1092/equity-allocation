const getUsers = "SELECT * FROM users WHERE tenant_id = $1";

const getUserByID = "SELECT * FROM users WHERE tenant_id = $1 and id = $2";

module.exports = {
    getUsers,
    getUserByID,
};