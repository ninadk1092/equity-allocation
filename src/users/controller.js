const pool = require('../../db');

const queries = require('./queries');

const getUsers = (req, res) => {
    const tenantID = parseInt(req.params.tenantID);
    pool.query(queries.getUsers, [tenantID], (error, results) => {
        if (error) throw error;
        res.status(200).json(results.rows);
    });
};

const getUserByID = (req, res) => {
    const tenantID = parseInt(req.params.tenantID);
    const id = parseInt(req.params.id);
    pool.query(queries.getUserByID, [tenantID, id], (error, results) => {
        if (error) throw error;
        res.status(200).json(results.rows);
    });
};

module.exports = {
    getUsers,
    getUserByID,
};