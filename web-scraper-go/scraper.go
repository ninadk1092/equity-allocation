package main

import (
	"fmt"
	"log"
	"strings"

	// importing Colly
	"github.com/gocolly/colly"
)

func main() {
	fmt.Println("Executing Visit() now")
	// scraping logic
	c := colly.NewCollector()

	c.OnRequest((func(r *colly.Request) {
		fmt.Println("Visiting: ", r.URL)
	}))

	c.OnError(func(r *colly.Response, err error) {
		log.Println("Something went wrong", err)
	})

	c.OnResponse(func(r *colly.Response) {
		fmt.Println("Page visited: ", r.Request.URL)
	})

	c.OnHTML("li.col-md-6", func(h *colly.HTMLElement) {
		fmt.Println(strings.TrimSpace(h.ChildText("a")))
		// stockList = h.ChildAttr("ul")
		// stockList.ForEach("li", func(j int, stockRow *colly.HTMLElement) {
		// 	stockRow.ForEach("a", func(k int, stockElem *colly.HTMLElement) {
		// 		fmt.Println(stockElem.Text)
		// 	})
		// })
	})

	c.OnScraped(func(r *colly.Response) {
		fmt.Println(r.Request.URL, " scraped!")
	})
	c.Visit("https://dhan.co/all-stocks-list/")
	fmt.Println("Done with Visit()")

	fmt.Println("Hello, World!")
}
